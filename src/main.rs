use clap::{
    App,
    Arg,
    SubCommand,
};

#[tokio::main]
async fn main() {
    let matches = App::new("mangarock")
        .subcommand(
            SubCommand::with_name("info")
                .about("Get info for a comic")
                .arg(
                    Arg::with_name("oid")
                        .takes_value(true)
                        .required(true)
                        .help("The OID of the comic"),
                ),
        )
        .subcommand(
            SubCommand::with_name("download")
                .about("Download a comic")
                .arg(Arg::with_name("oid").takes_value(true).required(true))
                .arg(
                    Arg::with_name("skip")
                        .short("s")
                        .long("skip")
                        .takes_value(true)
                        .help("Skip the first N chapters encountered"),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        ("info", Some(matches)) => {
            let oid = matches.value_of("oid").unwrap();
            let client = mangarock::Client::new();
            let series = client.get_series(&oid).await.expect("Error getting series");

            println!("Name: {}", series.name().unwrap());
            println!();
            for entry in series.iter_chapters().unwrap() {
                println!("Name: {}", entry.name());
                println!("Link: {}", entry.oid());
                println!();
            }
        }
        ("download", Some(matches)) => {
            let oid = matches.value_of("oid").unwrap();
            let skip = matches
                .value_of("skip")
                .and_then(|e| e.parse::<usize>().ok())
                .unwrap_or(0);
            let client = mangarock::Client::new();
            let series = client.get_series(&oid).await.expect("Error getting sereis");

            let series_name = series.name().unwrap();
            let chapters_len = series.chapters_len().unwrap();
            let chapters_iter = series.iter_chapters().unwrap();

            let mut p = std::path::PathBuf::from("comics");
            p.push("mangarock");
            p.push(&series_name);
            std::fs::create_dir_all(&p).unwrap();

            println!("Name: {}", series_name);
            println!();
            for (i, entry) in chapters_iter.enumerate().skip(skip) {
                let chapter_completion_str = format!("[{}/{}]", i + 1, chapters_len);
                println!(
                    "{}\tDownloading Chapter: {}",
                    chapter_completion_str,
                    entry.name()
                );

                p.push(entry.name());
                std::fs::create_dir_all(&p).unwrap();

                let chapter = client.get_chapter(entry.oid()).await.unwrap();
                let num_imgs = chapter.len();
                for (i, img) in chapter.iter().enumerate() {
                    let filename = format!("{}.png", i + 1);
                    p.push(&filename);

                    println!(
                        "{}[{}/{}]\t Downloading to {}.png...",
                        chapter_completion_str,
                        i + 1,
                        num_imgs,
                        p.display(),
                    );

                    let img = client.get_image(img.url()).await.unwrap();
                    img.save(&p).unwrap();

                    p.pop();
                }
                p.pop();
                println!();
            }
        }
        _ => (),
    }
}
