mod types;
pub use types::*;

use image::DynamicImage;
use serde_json::json;
use std::collections::HashMap;
use url::Url;

pub type RockResult<T> = Result<T, RockError>;

#[derive(Debug)]
pub enum RockError {
    Network,
    InvalidBody,
    InvalidImage,
}

pub struct Client {
    client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Client {
            client: reqwest::Client::new(),
        }
    }

    pub async fn get_series(&self, oid: &str) -> RockResult<Manga> {
        let loc = "United States";
        let url = format!(
            "https://api.mangarockhd.com/query/web401/manga_detail?country={}",
            loc
        );
        let mut res: ApiResponse<HashMap<String, Manga>> = self
            .client
            .post(&url)
            .json(&json! ({
                "oids": {
                    oid: 0
                },
                "sections": [
                    "basic_info",
                    "summary",
                    "artworks",
                    "sub_genres",
                    "social_stats",
                    "author",
                    "character",
                    "publisher",
                    "scanlator",
                    "other_fact",
                    "chapters",
                    "related_series",
                    "same_author",
                    "feature_collections"
                ]
            }))
            .send()
            .await
            .map_err(|_| RockError::Network)?
            .json()
            .await
            .map_err(|_| RockError::InvalidBody)?;

        res.data.remove(oid).ok_or(RockError::InvalidBody)
    }

    pub async fn get_chapter(&self, oid: &str) -> RockResult<Vec<ImageEntry>> {
        let country = "United States";
        let url = format!(
            "https://api.mangarockhd.com/query/web401/pagesv2?oid={}&country={}",
            oid, country
        );
        let res: ApiResponse<Vec<ImageEntry>> = self
            .client
            .get(&url)
            .send()
            .await
            .map_err(|_| RockError::Network)?
            .json()
            .await
            .map_err(|_| RockError::InvalidBody)?;
        Ok(res.data)
    }

    pub async fn get_image(&self, url: &Url) -> RockResult<DynamicImage> {
        let buf = self
            .client
            .get(url.as_str())
            .send()
            .await
            .map_err(|_| RockError::Network)?
            .bytes()
            .await
            .map_err(|_| RockError::InvalidBody)?;

        let webp_data = decode_mri(&buf).ok_or(RockError::InvalidImage)?;
        let (w, h, data) =
            libwebp::WebPDecodeRGBA(&webp_data).map_err(|_| RockError::InvalidImage)?;
        let rgba_image =
            image::RgbaImage::from_raw(w, h, data.to_vec()).ok_or(RockError::InvalidImage)?;
        Ok(image::DynamicImage::ImageRgba8(rgba_image))
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

fn decode_mri(arr: &[u8]) -> Option<Vec<u8>> {
    if arr[0] == 69 {
        let mut ret: Vec<u8> = vec![0; arr.len() + 15];
        let o = arr.len() + 7;
        ret[0] = 82;
        ret[1] = 73;
        ret[2] = 70;
        ret[3] = 70;
        ret[7] = (o >> 24 & 255) as u8;
        ret[6] = (o >> 16 & 255) as u8;
        ret[5] = (o >> 8 & 255) as u8;
        ret[4] = (o & 255) as u8;
        ret[8] = 87;
        ret[9] = 69;
        ret[10] = 66;
        ret[11] = 80;
        ret[12] = 86;
        ret[13] = 80;
        ret[14] = 56;

        for i in 0..arr.len() {
            ret[i + 15] = 101 ^ arr[i];
        }

        Some(ret)
    } else {
        None
    }
}
