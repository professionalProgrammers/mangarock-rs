use serde::Deserialize;
use url::Url;

#[derive(Debug, Deserialize)]
pub struct ApiResponse<T> {
    code: u32,
    pub data: T,
}

#[derive(Debug, Deserialize)]
pub struct Manga {
    basic_info: Option<BasicInfo>,
    chapters: Option<ChapterDataHolder>,
}

impl Manga {
    pub fn name(&self) -> Option<&str> {
        self.basic_info.as_ref().map(|el| el.name.as_str())
    }

    pub fn iter_chapters(&self) -> Option<std::slice::Iter<ChapterData>> {
        self.chapters.as_ref().map(|el| el.chapters.iter())
    }

    pub fn chapters_len(&self) -> Option<usize> {
        self.chapters.as_ref().map(|el| el.chapters.len())
    }
}

#[derive(Debug, Deserialize)]
struct BasicInfo {
    author: String,
    completed: bool,
    name: String,
    description: String,
    total_chapters: u32,
}

#[derive(Debug, Deserialize)]
struct ChapterDataHolder {
    chapters: Vec<ChapterData>,
}

#[derive(Debug, Deserialize)]
pub struct ChapterData {
    cid: u64,
    oid: String,
    name: String,
    order: u64,
    last_updated: u64,
}

impl ChapterData {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn oid(&self) -> &str {
        &self.oid
    }
}

#[derive(Debug, Deserialize)]
pub struct ImageEntry {
    role: Option<String>,
    url: Url,
}

impl ImageEntry {
    pub fn url(&self) -> &Url {
        &self.url
    }

    pub fn as_url_str(&self) -> &str {
        self.url.as_str()
    }
}
